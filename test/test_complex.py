import unittest

import torch as th
import numpy as np
import scipy as sp
from scipy import linalg as sp_linalg

from torch_cpu_strided_complex import cpp
from torch_cuda_strided_complex import cuda
from numpy.testing import *


def t2n(t):
    return t.detach().cpu().numpy()

device = th.device('cuda:0')


class TestComplexTensor(unittest.TestCase):

    def test_empty(self):
        c4 = th.empty((2, 2), dtype=th.complex64, device=device)
        c8 = th.empty((2, 2), dtype=th.complex128, device=device)

    def test_print(self):
        c4 = th.empty((2, 2), dtype=th.complex64, device=device)
        c8 = th.empty((2, 2), dtype=th.complex128, device=device)

        print(c4)
        print(c8)

    def test_copy(self):
        c4 = th.empty((2, 2), dtype=th.complex64, device=device)
        c8 = th.empty((2, 2), dtype=th.complex128, device=device)

        c4 = c4.clone()
        c8 = c8.clone()

    def test_tensor_factories(self):
        c4 = th.empty((2, 2), dtype=th.complex64, device=device)
        c8 = th.empty((2, 2), dtype=th.complex128, device=device)

        c4 = th.tensor([[1+1j, 1-1j], [3.-2.51j, 3.+2.51j]], dtype=th.complex64, device=device)
        c8 = th.tensor([[1+1j, 1-1j], [3.-2.51j, 3.+2.51j]], dtype=th.complex128, device=device)

        assert_allclose(t2n(c4.type(th.uint8)), t2n(c4).astype(np.uint8))
        assert_allclose(t2n(c8.type(th.uint8)), t2n(c8).astype(np.uint8))

        c4.type(th.uint8)
        c8.type(th.uint8)

        assert_allclose(t2n(th.full((2, 2), 1+1j, dtype=th.complex64, device=device)),
                        np.full((2, 2), 1+1j, dtype=np.complex64))
        assert_allclose(t2n(th.full((2, 2), 1+1j, dtype=th.complex128, device=device)),
                        np.full((2, 2), 1+1j, dtype=np.complex128))

        assert_allclose(t2n(th.ones((2, 2), dtype=th.complex64, device=device)),
                        np.ones((2, 2), dtype=np.complex64))
        assert_allclose(t2n(th.ones((2, 2), dtype=th.complex128, device=device)),
                        np.ones((2, 2), dtype=np.complex128))

        assert_allclose(t2n(th.scalar_tensor(1+1j, dtype=th.complex64, device=device)),
                        np.complex64(1+1j))
        assert_allclose(t2n(th.scalar_tensor(1 + 1j, dtype=th.complex128, device=device)),
                        np.complex128(1 + 1j))

        th.empty_like(c8)
        th.full_like(c8, 1.0+1.0j)
        th.ones_like(c8)
        # th.zeros_like(c8)

        # Port th_zero from TH to native
        # c4 = th.zeros((2, 2), dtype=th.complex64)  # zero not ported from TH
        # c8 = th.zeros((2, 2), dtype=th.complex128)  # zero not ported from TH

        # Port th_zero from TH to native
        # c4 = th.eye(2, dtype=th.complex64)  # zero not ported from TH
        # c8 = th.eye(2, dtype=th.complex128)  # zero not ported from TH

    def test_range_factories(self):
        assert_allclose(t2n(th.linspace(0 + 0j, 2 + 2j, 3, dtype=th.complex64, device=device)),
                        np.linspace(0 + 0j, 2 + 2j, 3, dtype=np.complex64, device=device))
        assert_allclose(t2n(th.linspace(0 + 0j, 2 + 2j, 3, dtype=th.complex128, device=device)),
                        np.linspace(0 + 0j, 2 + 2j, 3, dtype=np.complex128, device=device))

        assert_allclose(t2n(th.logspace(start=1 + 1j, end=1.1 + 1.1j, steps=5, dtype=th.complex64, device=device)),
                        np.logspace(1 + 1j, 1.1 + 1.1j, num=5, dtype=np.complex64), rtol=1e-6, atol=1e-5)
        assert_allclose(t2n(th.logspace(start=1 + 1j, end=1.1 + 1.1j, steps=5, dtype=th.complex128, device=device)),
                        np.logspace(1 + 1j, 1.1 + 1.1j, num=5, dtype=np.complex128))

        th.arange(0 + 0j, 2 + 2j, 1 + 1j, dtype=th.complex64, device=device)  # numpy doesn't support complex step
        th.arange(0 + 0j, 2 + 2j, 1 + 1j, dtype=th.complex128, device=device)  # numpy doesn't support complex step
        # c4 = th.range(0 + 0j, 2 + 2j, 1 + 1j, dtype=th.complex64)  # deprecated
        # c8 = th.range(0 + 0j, 2 + 2j, 1 + 1j, dtype=th.complex128)  # deprecated

    def test_scalar_ops(self):
        a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], dtype=th.complex128, device=device)

        assert_allclose(t2n(a + (3.51 + 3.51j)), t2n(a) + (3.51 + 3.51j))
        assert_allclose(t2n(a - (3.51 + 3.51j)), t2n(a) - (3.51 + 3.51j))
        assert_allclose(t2n(a * (3.51 + 3.51j)), t2n(a) * (3.51 + 3.51j))
        assert_allclose(t2n(a / (3.51 + 3.51j)), t2n(a) / (3.51 + 3.51j))

        assert_allclose(t2n((3.51 + 3.51j) + a), (3.51 + 3.51j) + t2n(a))
        assert_allclose(t2n((3.51 + 3.51j) - a), (3.51 + 3.51j) - t2n(a))
        assert_allclose(t2n((3.51 + 3.51j) * a), (3.51 + 3.51j) * t2n(a))
        assert_allclose(t2n((3.51 + 3.51j) / a), (3.51 + 3.51j) / t2n(a))

    def test_binary_ops(self):
        a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], dtype=th.complex128, device=device)

        assert_allclose(t2n(a + a), t2n(a) + t2n(a))
        assert_allclose(t2n(a - a), t2n(a) - t2n(a))
        assert_allclose(t2n(a * a), t2n(a) * t2n(a))
        assert_allclose(t2n(a / a), t2n(a) / t2n(a))

        assert_allclose(t2n(a == a), t2n(a) == t2n(a))
        assert_allclose(t2n(a != a), t2n(a) != t2n(a))

    def test_unary_ops(self):
        a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], dtype=th.complex128, device=device)

        # assert_allclose(t2n(th.abs(a)), np.abs(t2n(a)))
        assert_allclose(t2n(th.angle(a)), np.angle(t2n(a)))
        assert_allclose(t2n(th.real(a)), np.real(t2n(a)))
        assert_allclose(t2n(th.imag(a)), np.imag(t2n(a)))
        assert_allclose(t2n(th.conj(a)), np.conj(t2n(a)))

        #assert_allclose(th.sin(a), np.sin(t2n(a)))
        #assert_allclose(th.cos(a), np.cos(t2n(a)))
        #assert_allclose(th.tan(a), np.tan(t2n(a)))

        #assert_allclose(th.asin(a), np.arcsin(t2n(a)))
        #assert_allclose(th.acos(a), np.arccos(t2n(a)))
        #assert_allclose(th.atan(a), np.arctan(t2n(a)))

        #assert_allclose(th.sinh(a), np.sinh(t2n(a)))
        #assert_allclose(th.cosh(a), np.cosh(t2n(a)))
        #assert_allclose(th.tanh(a), np.tanh(t2n(a)))

        #assert_allclose(th.exp(a), np.exp(t2n(a)))
        assert_allclose(t2n(th.log(a)), np.log(t2n(a)))
        assert_allclose(t2n(th.log2(a)), np.log2(t2n(a)))
        assert_allclose(t2n(th.log10(a)), np.log10(t2n(a)))
        assert_allclose(t2n(th.sqrt(a)), np.sqrt(t2n(a)))

        #th.ceil(a)  # numpy does not support complex
        #th.floor(a)  # numpy does not support complex
        #assert_allclose(th.round(a), np.round(t2n(a)))
        #th.trunc(a)  # numpy does not support complex

        #th.sigmoid(a)  # no numpy function
        #th.frac(a)  # numpy does not support complex
        #assert_allclose(t2n(th.neg(a)), -1*(t2n(a)))
        #assert_allclose(t2n(th.reciprocal(a)), np.reciprocal(t2n(a)))

    def test_tensor_compare(self):
        a = th.tensor([[1+1j, 1-1j], [0.51, 0.51+0.51j]], dtype=th.complex128, device=device)

        #assert_allclose(th.max(a, -2)[0], np.max(t2n(a), axis=-2))
        #assert_allclose(th.argmax(a, -2), np.argmax(t2n(a), axis=-2))
        #assert_allclose(th.min(a, -2)[0], np.min(t2n(a), axis=-2))
        #assert_allclose(th.argmin(a, -2), np.argmin(t2n(a), axis=-2))

        #assert_allclose(th.where((a < a).type(th.uint8), a, a), np.where((t2n(a) < t2n(a)), t2n(a), t2n(a)))

        assert_allclose(th.isnan(a), np.isnan(t2n(a)))
        assert_allclose(th.isclose(a, a), np.isclose(t2n(a), t2n(a)))
        # th.allclose(a, a)  # all() not working
        # th.is_nonzero(a[0, 0])  # item() not working

    @unittest.expectedFailure
    def test_reduce_ops(self):
        a = th.tensor([[1+1j, 1-1j], [0.51, 0.51+0.51j]], dtype=th.complex128, device=device)

        assert_allclose(th.sum(a), np.sum(t2n(a)))
        # assert_allclose(th.prod(a), np.prod(t2n(a)))  # not equal
        assert_allclose(th.mean(a), np.mean(t2n(a)))
        # assert_allclose(th.std(a, 0), np.std(t2n(a), 0))  # not equal
        # assert_allclose(th.var(a, 0), np.var(t2n(a), 0))  # not equal

        assert_allclose(th.any(a.type(th.uint8) == a.type(th.uint8)), np.any(t2n(a) == t2n(a)))
        assert_allclose(th.all(a.type(th.uint8) == a.type(th.uint8)), np.all(t2n(a) == t2n(a)))

    def test_index_ops(self):
        a = th.tensor([[1+1j, 1-1j], [0.51, 0.51+0.51j]], dtype=th.complex128, device=device)

        assert_allclose(t2n(a[1, 0]), t2n(a)[1, 0])
        # a[1, 0] = 1.0 + 0.0j  # _th_set_ not implemented
        indices = th.tensor([0, 1])
        # th.take(a, indices)  # _th_set_ not implemented
        # th.index_select(a, 0, indices)  # _th_index_select not implemented

    def test_pointwise_ops(self):
        a = th.empty(2, 2, dtype=th.complex128, device=device)

        th.addcmul(a, 0.1 + 0.2j, a, a)  # numpy doesn't support addcmul
        th.addcdiv(a, 0.1 + 0.2j, a, a)  # numpy doesn't support addcmul

    @unittest.expectedFailure
    def test_lerp_ops(self):
        a = th.empty(2, 2, dtype=th.complex128, device=device)
        w = th.full((2, 2), 0.5, dtype=th.complex128, device=device)

        th.lerp(a, a, 0.5)
        th.lerp(a, a, w)

    @unittest.expectedFailure
    def test_linpack(self):

        # qr
        a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                       [6. + 6.j, 167. + 167j, -68. - 68.j],
                       [-4. - 4.j, 24. + 24.j, -41. - 41.j]], dtype=th.complex128, device=device)
        b = th.tensor([[0], [0], [0]], dtype=th.complex128, device=device)
        assert_allclose(th.qr(a)[0], np.linalg.qr(a)[0])
        assert_allclose(th.qr(a)[1], np.linalg.qr(a)[1])
        th.solve(b, a)
        th.inverse(a)

        # lu
        b = th.tensor([[0], [0], [0]], dtype=th.complex128, device=device)
        a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                       [6. + 6.j, 167. + 167j, -68. - 68.j],
                       [-4. - 4.j, 24. + 24.j, -41. - 41.j]], dtype=th.complex128, device=device)
        # a = th.stack((a, a), 0)
        # assert_allclose(th.lu(a)[0], sp.linalg.lu(a)[0]) # at::ScalarType::BFloat16 not implemented for 'Bool'
        # th.lu_solve(a, a, a)

        # cholesky
        a = th.tensor([[1, -2j],
                       [2j, 5]], dtype=th.complex128, device=device)
        b = th.tensor([[0], [0]], dtype=th.complex128, device=device)
        assert_allclose(th.cholesky(a), np.linalg.cholesky(a))
        assert_allclose(th.cholesky(a), np.linalg.cholesky(a))
        th.cholesky_solve(b, th.cholesky(a))
        # assert_allclose(th.cholesky_solve(b, th.cholesky(a)),
        #                 sp_linalg.cho_solve(np.linalg.cholesky(a)[0], b))

        # tri u/l
        b = th.tensor([[0], [0], [0]], dtype=th.complex128)
        a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                       [6. + 6.j, 167. + 167j, -68. - 68.j],
                       [-4. - 4.j, 24. + 24.j, -41. - 41.j]], dtype=th.complex128, device=device)
        assert_allclose(th.triu(a), sp.triu(a))
        assert_allclose(th.triangular_solve(b, th.triu(a), upper=True)[0],
                        sp_linalg.solve_triangular(sp.triu(a), b, lower=False))
        assert_allclose(th.tril(a), sp.tril(a))
        assert_allclose(th.triangular_solve(b, th.tril(a), upper=False)[0],
                        sp_linalg.solve_triangular(sp.tril(a), b, lower=True))

        # svd
        b = th.tensor([[0], [0], [0]], dtype=th.complex128)
        a = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                       [6. + 6.j, 167. + 167j, -68. - 68.j],
                       [-4. - 4.j, 24. + 24.j, -41. - 41.j]], dtype=th.complex128, device=device)
        # assert_allclose(th.svd(a), sp.svd(a))  # crashes due to complex.to(double) operation
        # th.pinverse(a) # see svd
        # th.matrix_rank(a) # see svd
        # th.trapz(a, a) # see svd

        a = th.tensor([[1, 0], [0, 1]], dtype=th.complex128, device=device)
        b = th.tensor([[0], [0], [0]], dtype=th.complex128, device=device)

        # th.geqrf(a)  # _th_geqrf not implemented
        # th.orgqr(a, a)  # _th_orgqr not implemented
        # th.ormqr(a, a, a)  # _th_ormqr not implemented

        # least squares
        # th.gels(b, a)  # _th_gels not implemented
        # th.lstsq(b, a)  # _th_gels not implemented

        # determinant
        # th.det(a)  # all only supports th.uint8 and th.bool dtypes
        # th.logdet(a)  # all only supports th.uint8 and th.bool dtypes
        # th.slogdet(a)  # all only supports th.uint8 and th.bool dtypes

    @unittest.expectedFailure
    def test_blas(self):
        v2 = th.empty((2,), dtype=th.complex128, device=device)
        v3 = th.empty((3,), dtype=th.complex128, device=device)
        m2 = th.empty((2, 2), dtype=th.complex128, device=device)
        m3 = th.tensor([[12. + 12.j, -51. - 51.j, 4. + 4.j],
                        [6. + 6.j, 167. + 167j, -68. - 68.j],
                        [-4. - 4.j, 24. + 24.j, -41. - 41.j]], dtype=th.complex128, device=device)

        assert_allclose(th.cross(m3, m3), np.cross(m3, m3))
        # th.ger(v2, v3)  # _th_ger not implemented
        # th.dot(m2, m2) # _th_dot not implemented
        # th.eig(m2)  # _th_eig not implemented
        # th.mv(m2, v2)  # _th_mv not implemented
        # th.matmul(m2, m2)  # _th_mm not implemented
        # th.mm(m2, m2)  # _th_mm not implemented
        # th.matrix_power(m2, 2)

    @unittest.expectedFailure
    def test_spectral_ops(self):
        a = th.tensor([[1+1j, 1-1j], [0.51, 0.51+0.51j]], dtype=th.complex128, device=device)

        def to_float(tensor):
            return th.stack((tensor.real().type(th.float64), tensor.imag().type(th.float64)), -1)

        def to_complex(tensor):
            tensor = tensor.type(th.complex128)
            return tensor[..., 0] + 1j*tensor[..., 1]

        assert_allclose(th.bartlett_window(100, dtype=th.complex128, device=device),
                        np.bartlett(100).astype(np.complex128), atol=0.05)
        assert_allclose(th.blackman_window(100, dtype=th.complex128, device=device),
                        np.blackman(100).astype(np.complex128), atol=0.05)
        assert_allclose(th.hamming_window(100, dtype=th.complex128, device=device),
                        np.hamming(100).astype(np.complex128), atol=0.05)
        assert_allclose(th.hann_window(100, dtype=th.complex128, device=device),
                        np.hanning(100).astype(np.complex128), atol=0.05)

        assert_allclose(to_complex(th.fft(to_float(a), 1)), np.fft.fft(t2n(a)))
        assert_allclose(to_complex(th.ifft(to_float(a), 1)), np.fft.ifft(t2n(a)))


if __name__ == '__main__':
    unittest.main()
